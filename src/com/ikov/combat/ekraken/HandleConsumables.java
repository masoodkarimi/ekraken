package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;

public class HandleConsumables implements Strategy {

	@Override
	public boolean activate() {
		return Skill.HITPOINTS.getLevel() < 60 || !Variables.drankOverload || !Variables.drankRenewal;
	}

	@Override
	public void execute() {

		if (Skill.HITPOINTS.getLevel() < 60) {
			Item i = getItem(Constants.SARADOMIN_BREW_IDS);
			Menu.sendAction(74, i.getId() - 1, i.getSlot(), 3214, 2213, 4);
			Time.sleep(1200);
		}

		if (!Variables.drankOverload) {
			Item i = getItem(ItemData.OVERLOAD.getIds());
			Menu.sendAction(74, i.getId() - 1, i.getSlot(), 3214, 2213, 4);
			Time.sleep(6000);
			Variables.drankOverload = true;
		}

		if (!Variables.drankRenewal) {
			Item i = getItem(Constants.RENEWAL_IDS);
			Menu.sendAction(74, i.getId() - 1, i.getSlot(), 3214, 2213, 4);
			Time.sleep(1200);
			Variables.drankRenewal = true;
		}

		if (Constants.KRAKEN_TILE.isOnMinimap()) {
			Variables.attackKraken = true;
		}
	}

	private Item getItem(int... ids) {
		Item[] i = Inventory.getItems(ids);
		if (i.length > 0) {
			if (i[0] != null) {
				Item i1 = i[0];
				return i1;
			}
		}
		return null;
	}
}

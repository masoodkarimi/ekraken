package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Npcs.Option;
import org.rev317.min.api.wrappers.Npc;


public class HandleCombat implements Strategy {

	@Override
	public boolean activate() {
		return Constants.KRAKEN_TILE.isOnMinimap() || Variables.attackKraken || Variables.specTimer.getElapsedTime() > 10000;
	}

	@Override
	public void execute() {

		if (Variables.attackKraken) {
			Npc n = Npcs.getClosest(Constants.KRAKEN_ID);
			if (n != null) {
				n.interact(Option.ATTACK);
				Time.sleep(2000);
				Variables.attackKraken = false;
			}

		}

		if (getNearest(Constants.WHIRLPOOL_ID).length > 0) {

			final Npc[] whirlpool = getNearest(Constants.WHIRLPOOL_ID);
			for (int i = 0; i < whirlpool.length; i++) {
				final Npc n = whirlpool[i];
				n.interact(Option.ATTACK);
				Time.sleep(new SleepCondition() {

					@Override
					public boolean isValid() {
						return n.getDef() == null;
					}

				}, 1500);
			}
		} else if (getNearest(Constants.KRAKEN_WHIRLPOOL_ID).length > 0) {
			Npc[] n = getNearest(Constants.KRAKEN_WHIRLPOOL_ID);

			if (n[0] != null) {
				Npc k = n[0];
				k.interact(Option.ATTACK);
				Time.sleep(new SleepCondition() {

					@Override
					public boolean isValid() {
						return getNearest(Constants.KRAKEN_ID).length > 0;
					}

				}, 3000);
			}
		}

		if (Variables.specTimer.getElapsedTime() > 10000 && getNearest(Constants.KRAKEN_ID).length > 0) {
			Menu.sendAction(315, 0, 0, 18023, 0);
			Variables.specTimer.restart();
			Time.sleep(1200);
		}

	}

	private Npc[] getNearest(int id) {

		return Npcs.getNearest(krakenFilter(id));

	}

	private final Filter<Npc> krakenFilter(final int id) {
		return new Filter<Npc>() {
			@Override
			public boolean accept(Npc n) {
				if (n != null && n.getDef().getId() == id && !n.isInCombat() && n.getInteractingCharacter() == null) {
					return true;
				}
				return false;
			}
		};
	}
}

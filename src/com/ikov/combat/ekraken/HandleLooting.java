package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.GroundItems.Option;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.wrappers.GroundItem;

public class HandleLooting implements Strategy {

	@Override
	public boolean activate() {
		return Variables.checkLoot;
	}

	@Override
	public void execute() {
		Time.sleep(1500);
		System.out.println("Checking loot");
		GroundItem[] toPickup = GroundItems.getNearest();

		if (toPickup.length > 0) {
			if (toPickup[0] != null) {
				Variables.checkLoot = false;
				Variables.reset = true;
				for (int i = 0; i < toPickup.length; i++) {
					if (toPickup[i].getId() == 12005 || toPickup[i].getId() == 12004 || toPickup[i].getId() == 11908 || toPickup[i].getId() == 11909) {
						if (!Inventory.isFull()) {
							toPickup[i].interact(Option.TAKE);
							Time.sleep(2000);
						}
					}
				}
			}
		}
	}
}

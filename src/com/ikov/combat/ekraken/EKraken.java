package com.ikov.combat.ekraken;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import org.parabot.core.Context;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Timer;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;


@ScriptManifest(author = "Empathy", category = Category.COMBAT, description = "Kills the Kraken Boss", name = "EKraken", servers = { "Ikov" }, version = 2.0)
public class EKraken extends Script implements MessageListener, Paintable {

	private ArrayList<Strategy> list = new ArrayList<Strategy>();
	private Timer timer = new Timer();
	
	public boolean onExecute() { 
		Context.getInstance().getRandomHandler().clearActiveRandoms();
        for (org.parabot.environment.scripts.randoms.Random random : Context.getInstance().getRandomHandler().getRandoms()){
            if (!random.getName().equalsIgnoreCase("login")){
            	System.out.println("Adding random: " + random.getName());
                Context.getInstance().getRandomHandler().setActive(random);
            }
        }
    	Context.getInstance().getRandomHandler().addRandom(new HandleLogin());
		list.add(new HandleAutocast());
		list.add(new HandleBanking());
		list.add(new HandleLooting());
		list.add(new HandleConsumables());
		list.add(new HandleTravel());
		list.add(new HandleCombat());
		provide(list);
		return true;
	}

	@Override
	public void messageReceived(MessageEvent m) {
		if (m.getType() == 0) {
			if (m.getMessage().contains("overload effect has")) {
				Variables.drankOverload = false;
			} else if (m.getMessage().contains("renewal has run")) {
				Variables.drankRenewal = false;
			} else if (m.getMessage().contains("achieved a")) {
				Variables.checkLoot = true;
				Variables.krakenKills++;
			}
		}
	}
	
	@Override
	public void onFinish() {
		System.out.println("---EKraken: Kraken Kills - " + Variables.krakenKills);
	}

	@Override
	public void paint(Graphics g1) {
		Graphics2D gr = (Graphics2D) g1;
		gr.setColor(Color.WHITE);
		gr.setFont(new Font("Verdana", 0, 12));
		gr.drawString("By: Empathy", 333, 120);
		gr.drawString("Runtime: " + timer.toString(), 333, 100);
		gr.drawString("Krakens Killed:  " + Variables.krakenKills + " (" + timer.getPerHour(Variables.krakenKills) + "/hr)", 333, 80);
		gr.drawRect(330, 6, 183, 130);
		Graphics2D rect = (Graphics2D) g1;
		rect.setColor(new Color(0, 0, 0, 120));
		rect.fillRect(330, 6, 183, 130);
	}
}
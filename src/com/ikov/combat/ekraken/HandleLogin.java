package com.ikov.combat.ekraken;

import java.awt.Point;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.randoms.Random;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;

public class HandleLogin implements Random {

	@Override
	public boolean activate() {
		return !Game.isLoggedIn() || Game.getOpenBackDialogId() == 4900;
	}

	@Override
	public void execute() {
		
		final Point p = new Point(516, 132);
		final Point p1 = new Point(380, 320);
		
		if (!Game.isLoggedIn()) {
			Time.sleep(4000);
			Mouse.getInstance().click(p);
			Time.sleep(1000);
			Mouse.getInstance().click(p1);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return Game.isLoggedIn();
				}
			}, 5000);
		}
		if (Game.getOpenBackDialogId() == 4900) {
			Time.sleep(1000);
			Menu.sendAction(679, 17825792, 113, 4907, 1088, 1);
			Time.sleep(500);
		}
	}

	@Override
	public String getName() {
		return "Log in";
	}

	@Override
	public String getServer() {
		return "Ikov";
	}
}

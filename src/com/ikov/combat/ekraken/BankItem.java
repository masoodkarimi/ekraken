package com.ikov.combat.ekraken;


import org.parabot.core.reflect.RefClass;
import org.parabot.core.reflect.RefField;
import org.parabot.environment.api.utils.Time;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

public final class BankItem {

	private static ItemData item;

	private static int[] getItemIds() {
		return item.getIds();
	}

	private static int getAmount() {
		return item.getAmount();
	}
	
	private static void setWithdrawAmount(int amount) {
		RefField withdrawAmount = new RefClass(Loader.getClient()).getField("q", "I");
		withdrawAmount.setInt(amount);
	}
	
	public static void setBankItem(ItemData inventory) {
		item = inventory;
	}

	public static void withdraw() {
		setWithdrawAmount(getAmount());
		if (getItemIds().length > 1) {
			for (int i = 0; i < getItemIds().length; i++) {
				Item z = Bank.getItem(getItemIds()[i]);
				if (z != null) {
					Menu.sendAction(776, z.getId() - 1, z.getSlot(), 5382, 2213, 5);
					Time.sleep(500);
					break;
				}
			}
		} else {
			Item i = Bank.getItem(getItemIds()[0]);
			if (i != null) {
				Menu.sendAction(776, i.getId() - 1, i.getSlot(), 5382, 2213, 5);
				Time.sleep(500);
			}
		}		
	}
	
	public static boolean hasItem() {
		return Inventory.getCount(true, getItemIds()) >= getAmount();
	}
}

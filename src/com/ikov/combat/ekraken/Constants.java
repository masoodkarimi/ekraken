package com.ikov.combat.ekraken;

import org.rev317.min.api.wrappers.Tile;

public class Constants {

	
	public static final int[] TELEPORT_OPTIONS = { 13087, 2498, 2498, 2498, 2498, 2495 };
	
	public static final Tile KRAKEN_TILE = new Tile(3696, 5806);
	
	public static final int WHIRLPOOL_ID = 15423, KRAKEN_WHIRLPOOL_ID = 15425, TENTACLE_ID = 15429, KRAKEN_ID = 15412;

	public static final Prayer[] PRAYERS = { Prayer.DEFLECT_MAGIC, Prayer.LEECH_DEFENCE, Prayer.LEECH_MAGIC };

	public static final int[] RENEWAL_IDS = { 14288, 15124, 15122, 15120, 7341, 14290 };
	
	public static final int[] SARADOMIN_BREW_IDS = { 14418, 14420, 14123, 14125, 14127, 14129 };
	
	public static final int[] LOOT_IDS = { 12005, 12004, 11908, 11909 };
	
}

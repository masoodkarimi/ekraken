package com.ikov.combat.ekraken;

public enum ItemData {
	
	LEGEND_SCROLL("Legend Scroll", 2000, 12826),
	OVERLOAD("Overload", 2, new int[] { 14302, 14300, 14298, 14296, 14292, 14292 }),
	PRAYER_RENEWAL("Prayer Renewal", 2, 14290),
	//SUPER_RESTORE("Super Restore", 3, 14416),
	SARADOMIN_BREW("Saradomin brew", 19, 14129),
	BLOOD_RUNE("Blood rune", 15000, 566),
	EARTH_RUNE("Earth rune", 15000, 558),
	SOUL_RUNE("Soul rune", 15000, 567)
	;
	
	ItemData(String name, int amount, int...ids) {
		this.name = name;
		this.amount = amount;
		this.ids = ids;
	}
	
	private String name;
	private int[] ids;
	private int amount;
	
	public String getName() {
		return name;
	}
	public int[] getIds() {
		return ids;
	}
	public int getAmount() {
		return amount;
	}
	
}

package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;


public class HandleTravel implements Strategy {

	@Override
	public boolean activate() {
		return Inventory.contains(14129) && !Constants.KRAKEN_TILE.isOnMinimap() || Variables.reset;
	}

	@Override
	public void execute() {
		
		for (int i = 0; i < Constants.PRAYERS.length; i++) {
			final Prayer p = Constants.PRAYERS[i];
			if (!p.isActivated()) {
				p.turnOn();
				Time.sleep(new SleepCondition() {

					@Override
					public boolean isValid() {
						return p.isActivated();
					}
					
				}, 3000);
			}
		}

		for (int i = 0; i < Constants.TELEPORT_OPTIONS.length; i++) {
			Menu.sendAction(315, 0, 0, Constants.TELEPORT_OPTIONS[i], 0, 1);
			Time.sleep(1200);
		}
		
		if (Variables.reset) {
			Variables.reset = false;
		}
	}
}

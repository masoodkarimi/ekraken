package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;

public class HandleAutocast implements Strategy {

	@Override
	public boolean activate() {
		return Game.getSetting(108) < 1 && Inventory.contains(567);
	}

	@Override
	public void execute() {

		Menu.sendAction(104, 2332, 491, 32660, 1276, 1);
		Time.sleep(new SleepCondition() {

			@Override
			public boolean isValid() {
				return Game.getSetting(108) > 0;
			}
			
		}, 5000);
		
	}
	

}

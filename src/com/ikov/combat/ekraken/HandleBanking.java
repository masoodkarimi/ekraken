package com.ikov.combat.ekraken;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Keyboard;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;

public class HandleBanking implements Strategy {

	@Override
	public boolean activate() {
		return Inventory.getCount(14129) <= 2 || !Variables.firstBank;
	}

	@Override
	public void execute() {
		if (Inventory.getCount(14129) <= 2) {
			System.out.println("Banking!");
			if (!Bank.isOpen() && Bank.getNearestBanks().length > 0 && Bank.getNearestBanks()[0].distanceTo() < 7) {

				Bank.open(Bank.getBank());
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Bank.isOpen();
					}
				}, 3000);
			} else {
				Keyboard.getInstance().sendKeys("::home");
				Time.sleep(4000);
			}

			if (Bank.isOpen()) {

				if (!Inventory.isEmpty() && !Inventory.isFull()) {
					Menu.clickButton(21012);
					Time.sleep(1000);

				}

				for (ItemData ie : ItemData.values()) {

					BankItem.setBankItem(ie);

					if (!BankItem.hasItem()) {
						BankItem.withdraw();
						Time.sleep(new SleepCondition() {

							@Override
							public boolean isValid() {
								return BankItem.hasItem();
							}

						}, 3000);
					}
				}

				if (Inventory.isFull()) {
					Bank.close();
				}

				if (!Variables.firstBank) {
					Variables.firstBank = true;
				}
			}
		}
	}
}
